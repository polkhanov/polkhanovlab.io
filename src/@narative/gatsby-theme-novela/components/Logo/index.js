import React from 'react';

export default function Logo({ fill }) {
    return (
        <svg width="19" height="26" viewBox="0 0 19 26" fill={fill} xmlns="http://www.w3.org/2000/svg">
            <path d="M2.17969 23.7656H14.8184V25.5938H0V0H2.17969V23.7656Z" fill={fill} />
            <path opacity="0.5" d="M18.9668 25.5938H16.7871V13.3242H2.16211V25.5938H0V0H2.16211V11.4961H16.7871V0H18.9668V25.5938Z" fill={fill} />
        </svg>
    );
}